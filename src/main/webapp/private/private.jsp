<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<%@ page pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Acces restreint avec filtre</title>
    </head>
    <body>
    <c:import url="/private/menu.jsp" /><hr>
     <h3> Espace prive Stagiaire</h3>
      
       <fieldset>
                <legend>Fiche information</legend>
               <table>
                <tr>
                	<td>
						<label for="nom">Votre ID:</label>
					</td>
					<td>
	                	<b><c:out value="${sessionScope.sessionPersonne.id}" /></b>
	                </td>
	            </tr>
               	 <tr>
                	<td>
						<label for="nom">Nom:</label>
					</td>
					<td>
	                	<b><c:out value="${sessionScope.sessionPersonne.nom}" /></b>
	                </td>
	            </tr>
	            <tr> 
	              <td>
	                <label for="nom">Prenom: </label>
	              </td>
	              <td>
	               <b> <c:out value="${sessionScope.sessionPersonne.prenom}"  /></b>
	              </td>
	            </tr>
	            <tr>
	              <td> 
	                <label for="email">Email : </label>
	              </td>
	              <td>
	                 <b> <c:out value="${sessionScope.sessionPersonne.email}"/></b>
	              </td>
	            </tr>
               </table>
            </fieldset>
            <br><br>
            <b>Liste des formations </b>
            
             <table  border="1">
      	<tr>
      		<td> Id Formation</td>
      		<td> libelle formation </td>
      		<td> Description</td>
      		<td> statut</td>
      		
      	</tr>
      	
      	
<%--       <c:forEach var="item" items="${applicationScope.formations}"> --%>
		    <c:forEach var="item" items="${requestScope.formations}">
      	<tr>
			<td><c:out value="${item.id}" /> </td>
			<td><c:out value="${item.label}" />  </td>
			<td><c:out value="${item.description}" /> </td>
			<td>
				
				<c:choose>
						<c:when test="${item.isInscrit }" > deja inscrit </c:when>
						<c:otherwise> <a href="<c:url value="/private/espaceMembre" > 
													<c:param name="idFormation" value="${item.id}"/>
											 </c:url>"> s'inscrire </a></c:otherwise>
				</c:choose></td>
		</tr>
		</c:forEach>
     </table>	
   			
   			
   			
    </body>
</html>