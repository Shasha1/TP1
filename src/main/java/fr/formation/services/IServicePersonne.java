package fr.formation.services;

import java.util.List;

import fr.formation.vo.PersonneVo;

public interface IServicePersonne {

	public boolean isPersonneExiste(PersonneVo personne);
	
	public PersonneVo recupererPersonne(PersonneVo persnne);
	
	public PersonneVo enregistrerPersonne(PersonneVo personneVo); 
	
	public List<PersonneVo> getAllPersonne();
}
