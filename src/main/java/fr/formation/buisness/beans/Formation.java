package fr.formation.buisness.beans;


public class Formation {
  private int id;
  private String label;
  private String description;
  private int idFormateur;
  private boolean isInscrit;
  private int nombreParticipant;
  
  
  

  
public int getNombreParticipant() {
	return nombreParticipant;
}
public void setNombreParticipant(int nombreParticipant) {
	this.nombreParticipant = nombreParticipant;
}
public boolean getIsInscrit() {
	return isInscrit;
}
public void setInscrit(boolean isInscrit) {
	this.isInscrit = isInscrit;
}


public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getLabel() {
	return label;
}
public void setLabel(String label) {
	this.label = label;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
public int getIdFormateur() {
	return idFormateur;
}
public void setIdFormateur(int idFormateur) {
	this.idFormateur = idFormateur;
}


  
}
