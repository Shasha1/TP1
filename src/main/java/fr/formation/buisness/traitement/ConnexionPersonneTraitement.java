package fr.formation.buisness.traitement;

import java.util.HashMap;
import java.util.Map;

import fr.formation.commons.ObjetsCommuns;
import fr.formation.vo.PersonneVo;

public class ConnexionPersonneTraitement extends ValidationChamps {
	
	private PersonneVo personne;
	
	
	public ConnexionPersonneTraitement(PersonneVo personne){
		this.personne = personne;
	}



	
	public boolean isValideChamps(){
		
	   
		  try {
				verifieChamps(personne.getEmail());
			} catch (Exception e) {
				mapErreur.put(ObjetsCommuns.Param.CHAMP_EMAIL, e.getMessage());
			}
		try {

			verifieChamps(personne.getMdp());
		} catch (Exception e) {
			mapErreur.put(ObjetsCommuns.Param.CHAMP_PASS, e.getMessage());
		}
	            
	
		return mapErreur.size()==0?true:false;
	}
	
	

}
