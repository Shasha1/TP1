package fr.formation.buisness.traitement;

import java.util.HashMap;
import java.util.Map;

public abstract class ValidationChamps {
	
	protected Map<String,String> mapErreur = new HashMap<String, String>();
	
	
	public Map<String, String> getErreurs() {
		return mapErreur;
	}
	
	
	protected void verifieChamps(String s) throws Exception{
		if(s.isEmpty()){
			throw new Exception("champs vide  ");
		}
		
		if( s.length()<3){
			throw new Exception("le nombre de caractere doit etre superieur a 3");
		}
		
	}
	
	protected void verifieMotsDePasse( String motDePasse, String confirmation ) throws Exception{
	    if (motDePasse != null && motDePasse.trim().length() != 0 && confirmation != null && confirmation.trim().length() != 0) {
	        if (!motDePasse.equals(confirmation)) {
	            throw new Exception("Les mots de passe entres sont differents, merci de les saisir a nouveau.");
	        } else if (motDePasse.trim().length() < 3) {
	            throw new Exception("Les mots de passe doivent contenir au moins 3 caracteres.");
	        }
	    } else {
	        throw new Exception("Merci de saisir et confirmer votre mot de passe.");
	    }
	}
	
	
	public abstract boolean isValideChamps();
	

}
