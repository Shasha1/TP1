package fr.formation.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.formation.commons.ObjetsCommuns;

public class DeconnexionServlet extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
			//Attention bien placer ce code ds le doGet et non doPost 
			HttpSession session = req.getSession();
		     session.invalidate();
//		     resp.sendRedirect( "www.google.fr" );
		     resp.sendRedirect( req.getContextPath() + "/connexion" );
		// this.getServletContext().getRequestDispatcher( ObjetsCommuns.Url.VUE_CONNEXION ).forward( req, resp );
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
	
		// resp.sendRedirect( req.getContextPath() + ObjetsCommuns.Url.VUE_CONNEXION );
	   this.getServletContext().getRequestDispatcher( ObjetsCommuns.Url.VUE_CONNEXION ).forward( req, resp );
	}
	

	
	
}
