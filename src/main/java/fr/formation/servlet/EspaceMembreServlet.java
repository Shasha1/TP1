package fr.formation.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.formation.buisness.beans.Formation;
import fr.formation.commons.ObjetsCommuns;
import fr.formation.dao.DaoImpl;
import fr.formation.services.IServiceFormation;
import fr.formation.services.ServiceFormateur;
import fr.formation.vo.PersonneVo;

public class EspaceMembreServlet extends HttpServlet {
	
	private IServiceFormation serviceFormation;
	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		 serviceFormation = new ServiceFormateur(DaoImpl.getInstance());
		 HttpSession session = req.getSession();
		 PersonneVo personne = (PersonneVo) session.getAttribute(ObjetsCommuns.SessionBeans.SESSION_PERSONNE);
		
		 String paramId = req.getParameter("idFormation");
		 
		 if(paramId!=null){
			 serviceFormation.inscriptionFormation(personne.getId(), Integer.parseInt(paramId));
		 }
			 
		
	    
		 List<Formation> catalogueFormation =  serviceFormation.recupererListeFormation();
		 
		 List<Formation> mesFormations =  serviceFormation.getListFormationParIdPersonne(personne.getId());
		 
		 Set<Integer> mesId = new HashSet<Integer>();
		 
		 if(mesFormations.size()>0){
			
			  session.setAttribute(ObjetsCommuns.SessionBeans.SESSION_FORMATIONS, mesFormations);
			 for(Formation f  : mesFormations){
				 mesId.add(f.getId());
			 }
		 }
		 
		
		 for(Formation f : catalogueFormation){
			 if(mesId.contains(f.getId())){
				 f.setInscrit(true);
			 }else{
				 f.setInscrit(false);
			 }
		 }
		 
		 //ici on set les info non pas ds la requete ms ds l'application scope !!!!!!=> ceci a titre dexemple
		 //cette liste de formations sera accessible pour tte lappli. N'est pas lie a la session !!! 
		 //getServletContext().setAttribute(ObjetsCommuns.Beans.ALL_FORMATION, catalogueFormation);
		
		 req.setAttribute(ObjetsCommuns.Beans.ALL_FORMATION , catalogueFormation); 
		 this.getServletContext().getRequestDispatcher( ObjetsCommuns.Url.VUE_ESPACE_PRIVE ).forward( req, resp );
		
	
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		 	
	    
	}
	

	
	
}
