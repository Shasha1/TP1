package fr.formation.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.formation.buisness.beans.Formation;
import fr.formation.buisness.beans.PersonneDao;
import fr.formation.commons.ObjetsCommuns;
import fr.formation.dao.DaoImpl;
import fr.formation.services.IServiceFormation;
import fr.formation.services.ServiceFormateur;
import fr.formation.vo.PersonneVo;

public class CreationFormationServlet extends HttpServlet {
	
	private IServiceFormation service; 
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		

		this.getServletContext().getRequestDispatcher( ObjetsCommuns.Url.VUE_CREATION_FORMATION ).forward( req, resp );
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
	
		 	String label = req.getParameter( ObjetsCommuns.Param.CHAMP_FORMATION_LABEL );
	        String desc = req.getParameter( ObjetsCommuns.Param.CHAMP_FORMATIONS_DESCRIPTION );
	       
	        HttpSession session = req.getSession();
		    PersonneVo personne = (PersonneVo) session.getAttribute(ObjetsCommuns.SessionBeans.SESSION_PERSONNE);
	       
	        
	        Formation formation = new Formation();
	        formation.setDescription(desc);
	        formation.setLabel(label);
	        formation.setIdFormateur(personne.getId());
	        
	        service = new ServiceFormateur(DaoImpl.getInstance());
	        service.creerFormation(formation);
	        
	        resp.sendRedirect( req.getContextPath() + ObjetsCommuns.Url.VUE_ESPACE_FORMATEUR );
			
	
	   
	}
	

	
	
}
