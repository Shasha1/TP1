package fr.formation.servlet;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.formation.buisness.beans.Formation;
import fr.formation.buisness.traitement.ConnexionPersonneTraitement;
import fr.formation.commons.ObjetsCommuns;
import fr.formation.dao.DaoImpl;
import fr.formation.services.IServiceFormation;
import fr.formation.services.IServicePersonne;
import fr.formation.services.ServiceFormateur;
import fr.formation.services.ServicePersonne;
import fr.formation.vo.PersonneVo;

public class ConnexionServlet extends HttpServlet {
	
	private IServicePersonne service;
	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		 /* Tentative de recuperation du cookie depuis la requete */
        String derniereConnexion = FactorySessionAndCookies.getCookieValue( req, ObjetsCommuns.Cookies.COOKIES_CONNECT );
       
        if ( derniereConnexion != null ) {
           
            req.setAttribute( ObjetsCommuns.Param.CHAMP_DATE_CONNECTION, derniereConnexion );
        }
    
		
		 this.getServletContext().getRequestDispatcher( ObjetsCommuns.Url.VUE_CONNEXION ).forward( req, resp );
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
	
		 	String email = req.getParameter( ObjetsCommuns.Param.CHAMP_EMAIL );
	        String motDePasse = req.getParameter( ObjetsCommuns.Param.CHAMP_PASS );
	       
	        PersonneVo p = new PersonneVo();
	        p.setEmail(email);
	        p.setMdp(motDePasse);
	        
	        ConnexionPersonneTraitement traitement = new ConnexionPersonneTraitement(p);
	        boolean isOk = traitement.isValideChamps();
	        
	        /* Recuperation de la session depuis la requete */
	        HttpSession session = req.getSession();
	        
	        PersonneVo personneRetour = null;
	       
	        service = new ServicePersonne(DaoImpl.getInstance());
	        personneRetour = service.recupererPersonne(p);
	        
	        if (isOk && personneRetour!=null ){
	        	System.out.println(personneRetour);
		        FactorySessionAndCookies.ajouterSession(session, req, resp, personneRetour);
	        	
	        }else{
	        	session.setAttribute(ObjetsCommuns.SessionBeans.SESSION_PERSONNE, null);
	        	FactorySessionAndCookies.supprimerSession(session, req, resp, p);
	        	   req.setAttribute(ObjetsCommuns.Param.CHAMP_MESSAGE , "Identifiant inconnu");  
	        }
	       
	        if ( req.getParameter(ObjetsCommuns.Param.CHAMP_MEMOIRE ) != null && personneRetour!=null ) {
	        	FactorySessionAndCookies.addCookies(resp);
	        } else {
	            /* Demande de suppression du cookie du navigateur */
	        	FactorySessionAndCookies.removeCookies(resp);
	        }
	        
	       
	    
		if(personneRetour!=null && personneRetour.isFormateur()){
			// req.setAttribute(ObjetsCommuns.Beans.PERSONNE_VO , personneRetour);        
			
			 //cette methode n'est pas la bonne car l'url reste inchange 
			 // this.getServletContext().getRequestDispatcher( ObjetsCommuns.Url.VUE_ESPACE_PRIVE_ADMIN ).forward( req, resp );
	
			 //cette appel est bonne ms pas tt a fait correcte car on a besoin + d'info donc il faut une nouvelle servlet
			 //on redirige donc vers la servlet grace a lurl pattern.
			 //la servlet recevra la req av la session correctement renseignee
			// resp.sendRedirect( req.getContextPath() + ObjetsCommuns.Url.VUE_ESPACE_PRIVE_ADMIN  );
			 resp.sendRedirect( req.getContextPath() + ObjetsCommuns.Url.VUE_ESPACE_FORMATEUR  );
			 
			 
		}
		else if (personneRetour!=null ){
			
		
			//req.setAttribute(ObjetsCommuns.Beans.PERSONNE_VO , personneRetour); 
			resp.sendRedirect( req.getContextPath() + ObjetsCommuns.Url.VUE_ESPACE_MEMBRE);
//			resp.sendRedirect( req.getContextPath() + ObjetsCommuns.Url.VUE_ESPACE_PRIVE  ); ici on ne peut pas passer en param les formations
			//this.getServletContext().getRequestDispatcher( ObjetsCommuns.Url.VUE_ESPACE_PRIVE ).forward( req, resp ); 
		}
		else{
			req.setAttribute(ObjetsCommuns.Beans.PERSONNE_VO , p);        
			req.setAttribute( ObjetsCommuns.Beans.MAP_ERREURS, traitement.getErreurs() );
			this.getServletContext().getRequestDispatcher( ObjetsCommuns.Url.VUE_CONNEXION ).forward( req, resp );
		}
	    
	}
	

	
	
}
